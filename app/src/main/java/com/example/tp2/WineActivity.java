package com.example.tp2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteException;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class WineActivity extends AppCompatActivity {

    EditText wineNameEdit;
    EditText wineRegionEdit;
    EditText locEdit;
    EditText climateEdit;
    EditText plantedAreaEdit;
    Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wine);

        Wine w = getIntent().getParcelableExtra("wine");
        final WineDbHelper dbh = new WineDbHelper(this);

        wineNameEdit = findViewById(R.id.wineName);
        wineRegionEdit = findViewById(R.id.editWineRegion);
        locEdit = findViewById(R.id.editLoc);
        climateEdit = findViewById(R.id.editClimate);
        plantedAreaEdit = findViewById(R.id.editPlantedArea);
        saveBtn = findViewById(R.id.button);

        // Remplissage des champs
        wineNameEdit.setText(w.getTitle());
        wineRegionEdit.setText(w.getRegion());
        locEdit.setText(w.getLocalization());
        climateEdit.setText(w.getClimate());
        plantedAreaEdit.setText(w.getPlantedArea());

        // Boutton de sauvegarde
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(wineNameEdit.getText().toString().isEmpty()) {
                    new AlertDialog.Builder(WineActivity.this)
                            .setTitle("Sauvegarde impossible")
                            .setMessage("Le nom du vin ne doit pas être vide").show();
                    return;
                }

                Cursor cursor = dbh.fetchAllWines();
                for (int i = 0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    System.out.println(cursor.getString(0));
                    if(cursor.getString(1).equals(wineNameEdit.getText().toString()) && cursor.getString(2).equals(wineRegionEdit.getText().toString())) {
                        new AlertDialog.Builder(WineActivity.this)
                                .setTitle("Ajout impossible")
                                .setMessage("Un vin existe déjà avec ce nom et cette région").show();
                        return;
                    }
                }

                Wine w = new Wine(wineNameEdit.getText().toString().trim(),
                        wineRegionEdit.getText().toString().trim(),
                        locEdit.getText().toString().trim(),
                        climateEdit.getText().toString().trim(),
                        plantedAreaEdit.getText().toString().trim());

                dbh.addWine(dbh.getWritableDatabase(), w);
                Toast.makeText(WineActivity.this, "Vin sauvegardé", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
