package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Parcelable;
import android.view.ContextMenu;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    ListView listview;
    SimpleCursorAdapter simpleCursorAdapter;
    WineDbHelper wineDbHelper;
    Cursor cursor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listview = (ListView) findViewById(R.id.listview);
        wineDbHelper = new WineDbHelper(this);
        registerForContextMenu(listview);

        // Toolbar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // -1 permet au wine activity de comprendre que c'est un nouveau wine
                Parcelable wineEmpty = new Wine(-1,"", "", "", "", "");
                Intent wineInfo = new Intent(MainActivity.this, WineActivity.class);
                wineInfo.putExtra("wine", wineEmpty);
                startActivity(wineInfo);
            }
        });

        // Remplissage de la liste
        fillListView();

        // Passage à la wine activity
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor.moveToPosition(position);
                Intent wineInfo = new Intent(MainActivity.this, WineActivity.class);
                Parcelable w = WineDbHelper.cursorToWine(cursor);
                wineInfo.putExtra("wine", w);
                startActivity(wineInfo);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.menu_suppress, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int listPosition = info.position;
        try {
            cursor.moveToPosition(listPosition);
            wineDbHelper.deleteWine(cursor);
            Toast.makeText(this, "Vin supprimé", Toast.LENGTH_SHORT).show();
            fillListView();
        }
        catch(Exception e){
            Toast.makeText(this, "Erreur: Le vin n'a pas pu être supprimé", Toast.LENGTH_SHORT).show();
        }
        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillListView();
    }

    public void fillListView() {
        cursor = wineDbHelper.fetchAllWines();
        simpleCursorAdapter = new SimpleCursorAdapter(this,
                R.layout.item_wine,
                cursor,
                new String[] {wineDbHelper.COLUMN_NAME, WineDbHelper.COLUMN_WINE_REGION},
                new int[] {R.id.wine_item_name, R.id.wine_item_desc});
        listview.setAdapter(simpleCursorAdapter);
    }
}
